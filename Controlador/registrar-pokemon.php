<?php  
	include('../Modelo/Pokemon.php');	
	include('../Modelo/TipoPokemon.php');
	
	$nombre = $_POST['nombre'];
	$tipo = $_POST['tipo'];
	$descripcion = $_POST['descripcion'];
	$ataqueEspecial = $_POST['ataque-especial'];
	$imagen = addslashes(file_get_contents($_FILES['imagen']['tmp_name']));

	$pokemon = new Pokemon($nombre,$descripcion,$ataqueEspecial,$imagen);
 	$pokemon->agregarPokemon();
 	$idPokemon = $pokemon->obtenerUltimoId();	
 	
 	foreach ($tipo as $tipos) {
		$tipoPokemon = new TipoPokemon($tipos,$idPokemon);
		$tipoPokemon->insertarPokemonYSusTipos();
	}
	header("location: ../Vista/agregar-pokemon.php");
	
?>