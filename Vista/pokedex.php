<!DOCTYPE html>
<html>
<head>
	<title>Pokedex</title>
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="../Comun/js/buscar-pokemon.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../Comun/css/Inicio.css">
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>

</head>
<body>
	<?php  
		include("../Comun/menu-pokedex.php");
	?>

	<section class="section-pokemon">
		
		<article class="art-buscar">
			<div class="div-buscar">
				
				<div class="div-cont-buscador">
					<input class="buscador" type="text" name="buscador" id="buscador">
					<button class="buscar icon-search3" id="buscar" name="buscar" value=""></button>
				</div>
			</div>
		</article>
	
		<article class="art-pokemon ">
			<div id="todos-pokemon" class="">
			<?php
				include("../Modelo/pokemon.php");
				include("../Modelo/TipoPokemon.php");	 
				$pokemon = new Pokemon();
				$pokemons = $pokemon->obtenerTodosLosPokemon();

				foreach ($pokemons as $pk) {
					echo "<div id='".$pk['nombre']."' class='nombre-pokemon'>
						  <a href='../Vista/mostrar-pokemon.php?idPokemon=".$pk['id_pokemon']."'>
						  <img src='data:image/jpg;base64,".base64_encode($pk['imagen'])."'></img>
						  <span class='id-poke'>Nro.".$pk['id_pokemon']."</span>
						  <p class='nombre-poke'>".$pk['nombre']."</p>
  						  <div class='los-tipos'><div class=''>";

						$tp = new TipoPokemon($pk['id_pokemon']); 
						$tipoPokemon = $tp->mostrarTiposDeUnPokemos();	  
						foreach ($tipoPokemon as $tps) {
							echo "<p class='tipo-poke'>".$tps['nombre']."</p>";
								  
						}
					echo "</div></div>";	
					echo "</a></div>";	
				}
			?>
			</div>
		</article>
	</section>
</body>
</html>
