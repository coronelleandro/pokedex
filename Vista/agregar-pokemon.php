<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>

	<script type="text/javascript" src="../Comun/js/pokedex-validar-formulario.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../Comun/css/agregar-pokemon.css">
</head>
<body>
	<?php  
	include("../Comun/menu-pokedex.php");
	include("../Modelo/tipo.php");
	?>
	<div class="cont-formulario">
		<form class="formulario" id="formulario" enctype="multipart/form-data" action="/pokedex/Controlador/registrar-pokemon.php" method="post">
			<div class="nombre">
				<label class="label-nombre" for="nombre"><p>Nombre</p></label>
				<input class="input-nombre" type="text" name="nombre" id="nombre" placeholder="Nombre">
			</div>	
			<div class="" id="error-nombre">
			</div>
			<div class="tipo">
				<label class="label-tipo" for="tipo"><p>Tipo</p></label>
				<select class="select-tipo" id="tipo" name="tipo[]" multiple="multiple">
				<?php
					 
					$tipo = new Tipo();
					$tipoPokemon = $tipo->seleccionarTodosLosTipos();

					foreach ($tipoPokemon as $tipos) {
						echo "<option value=".$tipos['id_tipo'].">".$tipos['nombre']."</option>";
					}
				?>
				</select>	
			</div>
			<div id="error-tipo" class=""></div>
			
			<div class="descripcion">
				<label class="label-desc" for="descripcion"><p>Descripcion</p></label>
				<textarea class="textarea-desc" name="descripcion" id="descripcion" placeholder="descripcion"></textarea>
			</div>
			<div id="error-descripcion" class=""></div>
			<div class="ataque-especial">
				<label class="label-ataque" for="ataque-especial"><p>Ataque Especial</p></label>
				<input class="input-ataque" type="text" id="ataque-especial" name="ataque-especial" placeholder="Ataque especial">
			</div>
			<div id="error-ataque-especial" class=""></div>
			<div class="imagen">
				<label class="label-imagen"  for="imagen"><p>Imagen</p></label>
				<input class="input-imagen" id="imagen" type="file" name="imagen">
			</div>
			<div id="error-imagen" class=""></div>
			<div class="registrar">
				<input class="input-registrar" type="submit" name="registrar" id="input-registrar" value="Registrar">
			</div>	
		</form>
	</div>
</body>
</html>
