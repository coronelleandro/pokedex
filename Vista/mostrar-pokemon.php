<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../Comun/css/mostrar-pokemon.css">
</head>
<body>
	<?php  
		include("../Comun/menu-pokedex.php");
		include("../Modelo/Pokemon.php");
		include("../Modelo/TipoPokemon.php");
	
	?>
	<section class="section-mp">
		<article class="article-mp">
			<?php  
				$id = $_GET['idPokemon'];
				$pokemon = new Pokemon($id);
				foreach ($pokemon->mostrarPokemon() as $pk) {
					echo "<h1 class='pokemon-nombre'>".$pk['nombre']."</h1>";	
					echo "<div class='content'>";
					echo "<div class='img-pk'>
						  <img src='data:image/jpg;base64,".base64_encode($pk['imagen'])."'></img>	
						  </div>";
					echo "<div class='desc-mp'>
						  <div class='desc'><p>".$pk['descripcion']."</p></div>
						  <div class='div-tipo'><p>Tipo:</p>
						  <div class='tipo'>";
					$tipoPokemon = new TipoPokemon($pk['id_pokemon']);
					foreach ($tipoPokemon->mostrarTiposDeUnPokemos() as $tp) {
						echo "<span>".$tp['nombre']."</span>";
					}
					echo "</div></div>";
					echo "</div>";
					echo "</div>";
					
				}
			?>
		</article>
	</section>
</body>
</html>