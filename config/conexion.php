<?php  

Class Conexion{
	
	private $host = 'localhost';
	private $user = 'root';
	private $pass = '';
	private $name = 'pokedex';  
	protected $conexion;

	function __construct(){
		$this->conexion = new mysqli($this->host,$this->user,$this->pass,$this->name);
		if($this->conexion->connect_errno){
			echo "Problemas con la conexion ";
		}else{
			$this->conexion->set_charset('utf8');
		}
	}

}

?>