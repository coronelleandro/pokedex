<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="/pokedex/Comun/css/menu.css">
	<link rel="stylesheet" type="text/css" href="/pokedex/Comun/fonts/style.css">
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!--<script type="text/javascript" src="js/menu.js"></script>-->
</head>
<body>
	<header>
		<nav class="menu">
			<div class="logo">
				<li><a href=""><img class="pokemon-logo" src="/pokedex/Comun/img/pokemon-logo.png"></a></li>
			</div>
			<div class="links">
				<li><a class="inicio" href="/pokedex/Vista/pokedex.php"><span class="icon-home2"></span>Inicio</a></li>
				<li><a class="agregar" href="/pokedex/Vista/agregar-pokemon.php"><span class="icon-squared-plus"></span>Agregar pokemon</a></li>
			</div>
			
				
			
		</nav>
	</header>
</body>
</html>
