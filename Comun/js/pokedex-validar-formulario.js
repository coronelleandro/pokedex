$(document).ready(function(){

	$(".formulario").submit(function(){

	var nombre = $("#nombre").val();
	var selectTipo = $("#tipo").val();
	var descripcion = $("#descripcion").val();
	var ataque_especial = $("#ataque-especial").val();
	var imagen = $("#imagen").val();
	//alert($("#imagen")val());
	var rtn = true;
	

	// ---- Nombre ----
	$("#error-nombre").html("<span class=''>EL nombre es obligatorio</span>");
	$("#error-nombre span").hide();
	if(nombre===""){
		$("#error-nombre span").show();
		rtn = false;
	}

	$("#error-tipo").html("<span class=''>Debe seleccionar un tipo</span>");
	$("#error-tipo span").hide();
	if(selectTipo.length<1){
		$("#error-tipo span").show();
		rtn = false;
	}

	$("#error-descripcion").html("<span class=''>La descripcion debe contar con mas de 10 caracteres</span>");
	$("#error-descripcion span").hide();
	if(descripcion.length<11){
		$("#error-descripcion span").show();
		rtn = false;
	}

	$("#error-ataque-especial").html("<span class=''>El ataque especial es obligatoria</span>");
	$("#error-ataque-especial span").hide();
	if(ataque_especial==""){
		$("#error-ataque-especial span").show();
		rtn = false;
	}

	$("#error-imagen").html("<span class=''>Debe agregar una imagen obligatoriamente</span>");
	$("#error-imagen span").hide();
	if(imagen==""){
		$("#error-imagen span").show();
		rtn = false;
	}

	return rtn;	
	});
});