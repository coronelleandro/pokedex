<?php  

Class TipoPokemon extends Conexion{

	private $idTipo;
	private $idPokemon;

	public function __construct(){
		parent:: __construct();
		
		$args = func_get_args();
		if(count($args)==2){
			$this->idTipo = $args[0];
			$this->idPokemon = $args[1];

		}else if(count($args)==1){
			$this->idPokemon = $args[0];
		}
	}	

	public function insertarPokemonYSusTipos(){
		$this->conexion->query("INSERT INTO tipo_pokemon(id_tipo,id_pokemon)
								VALUES('$this->idTipo','$this->idPokemon')");
	} 


	public function mostrarTiposDeUnPokemos(){
		$consulta = $this->conexion->query("SELECT nombre FROM tipo_pokemon 
											INNER JOIN tipo on tipo_pokemon.id_tipo = tipo.id_tipo 
											WHERE id_pokemon='$this->idPokemon'");
		$resultado = $consulta->fetch_all(MYSQLI_ASSOC);
		return $resultado;
	}
}

?>