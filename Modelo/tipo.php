<?php  

include("../config/conexion.php");

Class Tipo extends Conexion{

	private $id_tipo;
	private $nombre;

	public  function Tipo(){
		parent:: __construct(); 
	}

	function seleccionarTodosLosTipos(){
		$consulta = $this->conexion->query("SELECT * FROM tipo");
		$tipos = $consulta->fetch_all(MYSQLI_ASSOC);
		return $tipos;
	}		
}
?>