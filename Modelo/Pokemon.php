<?php  
include("../config/conexion.php");

Class Pokemon extends Conexion{

	private $idPokemon;
	private $nombre;
	private $descripcion;
	private $ataqueEspecial;
	private $imagen;


	public function __construct(){
		parent:: __construct();
		$args = func_get_args();
		if(count($args)==4){
			$this->nombre = $args[0];
			$this->descripcion = $args[1];
			$this->ataqueEspecial = $args[2];
			$this->imagen = $args[3];

		}else if (count($args)==1){
			$this->idPokemon = $args[0];
		}
	}

	public function agregarPokemon(){
		$consulta = $this->conexion->query("INSERT INTO pokemon(nombre,descripcion,ataque_especial,imagen)
											VALUES('$this->nombre','$this->descripcion','$this->ataqueEspecial','$this->imagen')");
	}

	public function obtenerUltimoId(){
		$consulta = $this->conexion->query("SELECT max(id_pokemon)as elPokemon FROM pokemon LIMIT 1");
		$numero = $consulta->fetch_all(MYSQLI_ASSOC);
		$this->idPokemon = $numero[0]["elPokemon"];
		return $this->idPokemon;
	}

	public function obtenerTodosLosPokemon(){
		$consulta = $this->conexion->query("SELECT * FROM pokemon");
		$resultado = $consulta->fetch_all(MYSQLI_ASSOC);
		return $resultado;
	}

	public function mostrarPokemon(){
		$consulta = $this->conexion->query("SELECT * FROM pokemon WHERE id_pokemon='$this->idPokemon'");
		$resultado = $consulta->fetch_all(MYSQLI_ASSOC);
		return $resultado;
	}
}

?>